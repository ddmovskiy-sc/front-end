import React, { Component } from "react";
export default class Header extends Component {
  render() {
    return (
      <tr className = "p-3 mb-2 bg-info text-white">
        <th><input type = "checkbox" onClick = {this.props.onSelectAll}/>
          <div className = "spaceBeforeText">Select all</div>
        </th>
        <th onClick={() => this.props.onSortBy('name')}>Name product</th>
        <th onClick={() => this.props.onSortBy('weight')}>Weight</th>
        <th onClick={() => this.props.onSortBy('price')}>Price, $</th>
      </tr>
    )
  }
}