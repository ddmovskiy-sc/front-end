import React, { Component } from "react";
import logo from '../../resources/img/logo.png';
const STYLE = "form-control mr-sm-2";
export default class SearchBar extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-light bg-light justify-content-between">
          <a href="http://127.0.0.1:3000" className="navbar-brand"><img src={logo} alt={"logo"}/>Menu</a>
          <div className="btn-group-sm btn-group">
            <input onKeyUp={this.props.onSearch} className={STYLE} type="search" placeholder="Search" aria-label="Search" />
          </div>
        </nav>
      </div>
    )
  }
}