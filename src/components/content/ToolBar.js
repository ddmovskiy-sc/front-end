import React, { Component } from "react";
const STYLE_BUTTON = "btn btn-outline-secondary background-hover-btn btn-size-color";
export default class ToolBar extends Component {
  render() {
    return (
      <div className = "btn-group-lg d-flex justify-content-center">
        <button onClick = {this.props.onCalculate} className = {STYLE_BUTTON}>
          Calculate
        </button>
        <button onClick = {this.props.onReset} className = {STYLE_BUTTON}>
          Reset
        </button>
      </div>
    )
  }
}