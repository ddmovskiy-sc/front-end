import React, { Component } from "react";
import './css/style.min.css';
import data from '../../resources/files/data';
import Footer from './Footer';
import SearchBar from './SearchBar';
import ToolBar from './ToolBar';
import Header from './Header';
export default class Content extends Component {
  state = {
    btnCalculate: false,
    btnReset: false,
    sum: 0,
    items: [],
    showProducts: data,
    sortedField: null,
    isAscSortDirection: false
  }
  handleSearch = (event) => {
    const products = this.state.showProducts;
    const searchQuery = event.target.value.toLowerCase();
    const afterFilter = products.filter(products =>
      products.name.toLowerCase().indexOf(searchQuery) !== -1 ||
      products.weight.toLowerCase().indexOf(searchQuery) !== -1 ||
      products.price.toLowerCase().indexOf(searchQuery) !== -1);
    this.setState((state) => ({
      showProducts: state.showProducts = afterFilter
    }))
    if (event.target.value.length === 0) {
      this.setState((state) => ({
        showProducts: state.showProducts = data
      }))
    }
  }
  compareBy = (key) => {
    const isAscSortDirection = this.state.isAscSortDirection;
    return function(a, b) {
      const result = a[key] < b[key] ? 1 : (a[key] > b[key] ? -1 : 0);
      return isAscSortDirection ? result : -result;
    };
  }
  handleSortBy = (key) => {
    if (this.state.sortedField !== key) {
      this.setState({
        showProducts: this.state.showProducts,
        sortedField: key,
        isAscSortDirection: true
      })
    } else {
      this.setState({
        showProducts: this.state.showProducts,
        sortedField: key,
        isAscSortDirection: this.state.isAscSortDirection === true ? false : true
      })
    }
    const arrayCopy = [...this.state.showProducts];
    arrayCopy.sort(this.compareBy(key));
    this.setState({ showProducts: arrayCopy });
  }
  handleSetCheckbox = (event) => {
    this.state.items.push(event.target);
    const val = parseInt(event.target.value, 10);
    this.setState((state) => ({
      sum: state.sum = state.sum + val
    }))
  }
  handleCalculate = () => {
    this.setState((state) => ({
      btnCalculate: state.btnCalculate = true,
      btnReset: state.btnReset = false
    }))
  }
  handleReset = () => {
    if (this.state.items.length > 0) {
      this.state.items.forEach((item) => {
        item.checked = false;
      })
    }
    this.setState((state) => ({
      btnReset: state.btnReset = true,
      btnCalculate: state.btnCalculate = false,
      sum: state.sum = 0
    }))
  }
  handleSelectAll = (event) => {
    const nodeList = document.getElementsByTagName("input");
    let sumAllCheckbox = 0;
    const _items = [];
    if (event.target.checked === true) {
      _items.push(event.target);
      for (const key in nodeList) {
        if (nodeList[key].type === "checkbox" && isFinite(parseInt(nodeList[key].value, 10))) {
          sumAllCheckbox += +nodeList[key].value;
          nodeList[key].checked = true;
          _items.push(nodeList[key]);
        }
      }
      this.setState((state) => ({
        items: state.items = _items,
        sum: state.sum = sumAllCheckbox
      }))
    }
    if (event.target.checked === false) {
      for (const key in nodeList) {
        if (nodeList[key].type === "checkbox") {
          nodeList[key].checked = false;
        }
      }
      this.setState((state) => ({
        sum: state.sum = sumAllCheckbox,
        items: state.items = []
      }))
    }
  }
  render() {
    const resultCalculate = this.state.btnCalculate &&
     <div className = "calc-result">
       Total cost: {this.state.sum}$
     </div>
    const products = this.state.showProducts.map((detail, index) =>
      <tr key = {index}>
        <td>
          <input type = "checkbox" name = {detail.name} value = {detail.price} onChange = {this.handleSetCheckbox}/>
        </td>
        <td>{detail.name}</td>
        <td>{detail.weight}</td>
        <td>{detail.price}</td>
      </tr>
    )
    return (
      <div>
        <SearchBar onSearch = {this.handleSearch} />
        <div className = "table-wrapper-scroll-y">
          <table className = "table table-striped table-hover table-bordered">
            <thead>
              <Header onSelectAll = {this.handleSelectAll} onSortBy = {this.handleSortBy} />
            </thead>
            <tbody>
              {products}
            </tbody>
          </table>
        </div>
        <ToolBar onCalculate = {this.handleCalculate} onReset = {this.handleReset} />
        <Footer result = {resultCalculate} />
      </div>
    )
  }
}