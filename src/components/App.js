import React from "react";
import Content from './content/Content';
export default function App() {
  return (
    <div className="page-container">
      <main>
        <Content />
      </main>
    </div>
  )
}
